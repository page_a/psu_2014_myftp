##
## Makefile for myftp in /home/page_a/rendu/PSU_2014_myftp
## 
## Made by Alexandre PAGE
## Login   <page_a@epitech.net>
## 
## Started on  Mon Mar  9 10:00:54 2015 Alexandre PAGE
## Last update Sun Mar 22 01:21:18 2015 Alexandre PAGE
##

CLIENT		= client

SERVER		= serveur

CLIENT_FOLDER	= src_client

SERVER_FOLDER	= src_serveur

CLIENT_SRCS	= $(CLIENT_FOLDER)/client.c \
		  $(CLIENT_FOLDER)/list.c \
		  $(CLIENT_FOLDER)/my_str_to_wordtab.c \
		  $(CLIENT_FOLDER)/init.c \
		  $(CLIENT_FOLDER)/user.c \
		  $(CLIENT_FOLDER)/pwd_exit.c \
		  $(CLIENT_FOLDER)/get.c \
		  $(CLIENT_FOLDER)/put.c \
		  $(CLIENT_FOLDER)/pasv.c \
		  $(CLIENT_FOLDER)/noop.c \
		  $(CLIENT_FOLDER)/cwd.c \
		  $(CLIENT_FOLDER)/cdup.c \
		  $(CLIENT_FOLDER)/help.c \
		  $(CLIENT_FOLDER)/dele.c

SERVER_SRCS	= $(SERVER_FOLDER)/serveur.c \
		  $(SERVER_FOLDER)/serveur_fork.c \
		  $(SERVER_FOLDER)/ptrfunc.c \
		  $(SERVER_FOLDER)/cwd.c \
		  $(SERVER_FOLDER)/my_str_to_wordtab.c \
		  $(SERVER_FOLDER)/pwd.c \
		  $(SERVER_FOLDER)/quit.c \
		  $(SERVER_FOLDER)/login.c \
		  $(SERVER_FOLDER)/list.c \
		  $(SERVER_FOLDER)/pasv.c \
		  $(SERVER_FOLDER)/get.c \
		  $(SERVER_FOLDER)/put.c \
		  $(SERVER_FOLDER)/noop.c \
		  $(SERVER_FOLDER)/cdup.c \
		  $(SERVER_FOLDER)/help.c \
		  $(SERVER_FOLDER)/dele.c

CLIENT_OBJS	= $(CLIENT_SRCS:.c=.o)

SERVER_OBJS	= $(SERVER_SRCS:.c=.o)

RM		= rm -f

CC		= gcc

CFLAGS		= -W -Wall -Wextra

$(CLIENT): $(CLIENT_OBJS) $(SERVER_OBJS)
	$(CC) $(CLIENT_OBJS) -o $(CLIENT)
	$(CC) $(SERVER_OBJS) -o $(SERVER)

all: $(CLIENT)

clean:
	$(RM) $(SERVER_OBJS)
	$(RM) $(CLIENT_OBJS)

fclean: clean
	$(RM) $(CLIENT)
	$(RM) $(SERVER)

re: fclean all
