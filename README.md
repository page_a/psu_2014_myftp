# myftp #

Server and client for ftp protocol.

# Here are the implemented function
* CDUP -> Change to parent directory
* CWD -> Change working directory
* DELE -> Delete a file
* RETR -> Retrieve a copy of a file
* HELP -> Return a documentation
* LIST -> Retrieve information of current directory or the one specified in arg
* NOOP -> No operation used for keepalive
* PASV -> Enter passive mode 
* STOR -> Accept the data and store it (Accept upload)
* PWD -> Print current directory
* QUIT -> disconnect, quit
* PASS | USER (Authentification part)