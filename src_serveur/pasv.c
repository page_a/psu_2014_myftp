/*
** pasv.c for myftp in /home/page_a/rendu/PSU_2014_myftp/serveur
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Thu Mar 19 14:09:55 2015 Alexandre PAGE
** Last update Thu Mar 19 14:59:29 2015 Alexandre PAGE
*/

#include "serveur.h"

extern int	g_socketfd;

static void	replace_ip(char *str)
{
  int		i;

  i = 0;
  while (str[i])
    {
      if (str[i] == '.' && str[i + 1] && str[i + 1] != '\n')
	str[i] = ',';
      i = i + 1;
    }
}

static char	*str_enter_passive(t_param *param, int random,
				   int random2, int client_fd)
{
  char		*client_ip;
  char		*str;
  char		snum[5];

  client_ip = inet_ntoa(param->tmp.s_in_client.sin_addr);
  replace_ip(client_ip);
  if ((str = malloc(sizeof(*str) * 35 + strlen(param->client_ip))) == NULL)
    xexit(client_fd);
  strcpy(str, "227 Entering Passive Mode (");
  strcat(str, client_ip);
  strcat(str, ",");
  sprintf(snum, "%d", random);
  strcat(str, snum);
  strcat(str, ",");
  sprintf(snum, "%d", random2);
  strcat(str, snum);
  strcat(str, ").\n");
  xwrite(1, str);
  return (str);
}

void		pasv(int client_fd, char *buffer, t_param *param)
{
  (void)	buffer;
  int		port;
  socklen_t	len;
  int		random;
  int		random2;
  char		*tmp;

  len = sizeof(param->tmp.s_in_size);
  g_socketfd = init_socket(&param->tmp, 0);
  if ((getsockname(g_socketfd, (struct sockaddr *) &param->tmp.s_in,
		   &len)) == -1)
    xexit(client_fd);
  port = ntohs(param->tmp.s_in.sin_port);
  random = port >> 8;
  random2 = port % 256;
  tmp = str_enter_passive(param, random, random2, client_fd);
  xwrite(client_fd, tmp);
  free(tmp);
}
