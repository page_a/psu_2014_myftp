/*
** cd.c for myftp in /home/page_a/rendu/PSU_2014_myftp/serveur
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Tue Mar 17 17:08:24 2015 Alexandre PAGE
** Last update Sat Mar 21 21:35:11 2015 Alexandre PAGE
*/

#include "serveur.h"

void	epure_newline(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] == '\n')
	str[i] = '\0';
      i = i + 1;
    }
}

static char	*get_cwd(int client_fd, char *arg)
{
  char		*tmp;
  char		path[1024];

  getcwd(path, sizeof(path));
  if ((tmp = malloc(sizeof(*tmp) * strlen(path) + strlen(arg) + 1)) == NULL)
    xexit(client_fd);
  strcpy(tmp, path);
  strcat(tmp, "/");
  strcat(tmp, arg);
  strcat(tmp, "/");
  return (tmp);
}

static int	check_cwd_root(int client_fd, char *arg)
{
  char		*pwd;
  int		i;
  int		a;

  if ((pwd = getenv("PWD")) == NULL)
    xexit(client_fd);
  i = strlen(pwd);
  a = strlen(arg);
  if (i >= a)
    {
      if (strncmp(pwd, arg, strlen(pwd)))
	return (-1);
    }
  return (1);
}

static void	get_cwd_good(int client_fd, char *my_realpath)
{
  if (check_cwd_root(client_fd, my_realpath) == 1)
    {
      if (chdir(my_realpath) == -1)
	xwrite(client_fd, "550 Failed to change directory.\n");
      else
	xwrite(client_fd, "250 Directory successfully changed.\n");
    }
  else
    xwrite(client_fd, "250 Directory successfully changed.\n");
}

void		cwd(int client_fd, char *buffer, t_param *param)
{
  char		**arg;
  char		*tmp;
  char		*my_realpath;
  (void)	param;

  my_realpath = NULL;
  arg = my_str_to_wordtab(buffer, ' ');
  if (wordtab_len(arg) == 2)
    {
      tmp = get_cwd(client_fd, arg[1]);
      if ((my_realpath = realpath(tmp, my_realpath)) == NULL)
	xwrite(client_fd, "550 Failed to change directory.\n");
      else
	get_cwd_good(client_fd, my_realpath);
      free(tmp);
    }
  else
    xwrite(client_fd, "550 Failed to change directory.\n");
  free_str_to_wordtab(arg);
}
