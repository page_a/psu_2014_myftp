/*
** transfer.c for myftp in /home/page_a/rendu/PSU_2014_myftp/serveur
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Thu Mar 19 18:48:28 2015 Alexandre PAGE
** Last update Sat Mar 21 17:12:27 2015 Alexandre PAGE
*/

#include "serveur.h"

extern int	g_socketfd;

char		*get_filename(char *buffer)
{
  char		**arg;
  char		*str;

  arg = my_str_to_wordtab(buffer, ' ');
  if (wordtab_len(arg) >= 2)
    {
      str = strdup(arg[1]);
      free_str_to_wordtab(arg);
      return (str);
    }
  else
    {
      free_str_to_wordtab(arg);
      return (NULL);
    }
}

static void	getting_file(int client_fd, int tmp_fd, int fd)
{
  char		buff[BUFFER_DU];
  int		my_read;

  xwrite(client_fd,
	 "150 Opening ASCII mode data connection\n");
  while ((my_read = read(fd, buff, sizeof(buff))) > 0)
    write(tmp_fd, buff, my_read);
  xwrite(client_fd, "226 Transfer complete.\n");
}

void		get(int client_fd, char *buffer, t_param *param)
{
  int		tmp_fd;
  int		fd;
  char		*filename;

  if (!g_socketfd)
    xwrite(client_fd, "425 Use PASV first.\n");
  else
    {
      tmp_fd = accept(g_socketfd, (struct sockaddr *)&param->my.s_in_client,
		      &param->my.s_in_size);
      if (tmp_fd == -1)
	xexit(client_fd);
      if ((filename = get_filename(buffer)) == NULL)
	xexit(client_fd);
      if ((fd = open(filename, O_RDONLY)) == -1)
	xexit(client_fd);
      getting_file(client_fd, tmp_fd, fd);
      close(tmp_fd);
      close(g_socketfd);
      g_socketfd = 0;
    }
}
