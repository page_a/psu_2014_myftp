/*
** help.c for myftp in /home/page_a/rendu/psu_2014_myftp/serveur
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Sun Mar 22 00:50:15 2015 Alexandre PAGE
** Last update Sun Mar 22 00:54:21 2015 Alexandre PAGE
*/

#include "serveur.h"

void		help(int client_fd, char *buffer, t_param *param)
{
  (void)	buffer;
  (void)	param;

  xwrite(client_fd, "214-The following commands are recognized.\n");
  usleep(1000);
  xwrite(client_fd, "ls quit pwd user cd NOOP DELE put get\n");
  usleep(1000);
  xwrite(client_fd, "214 Help OK.\n");
}
