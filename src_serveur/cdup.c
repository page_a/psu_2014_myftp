/*
** cdup.c for myftp in /home/page_a/rendu/psu_2014_myftp/serveur
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Sun Mar 22 00:46:56 2015 Alexandre PAGE
** Last update Sun Mar 22 00:49:57 2015 Alexandre PAGE
*/

#include "serveur.h"

void		cdup(int client_fd, char *buffer, t_param *param)
{
  (void)	buffer;

  cwd(client_fd, "cd ..", param);
}
