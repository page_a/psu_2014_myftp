/*
** dele.c for myftp in /home/page_a/rendu/psu_2014_myftp/serveur
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Sun Mar 22 00:55:35 2015 Alexandre PAGE
** Last update Sun Mar 22 01:08:21 2015 Alexandre PAGE
*/

#include "serveur.h"

void		dele(int client_fd, char *buffer, t_param *param)
{
  char		*filename;
  (void)	param;

  if ((filename = get_filename(buffer)) != NULL)
    {
      if (remove(filename) == -1)
	xwrite(client_fd, "550 Requested action not taken.\n");
      else
	xwrite(client_fd, "250 Requested file action okay, completed.\n");
    }
}
