/*
** my_str_to_wordtab.c for myftp in /home/page_a/rendu/PSU_2014_myftp
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Wed Mar 11 21:11:28 2015 Alexandre PAGE
** Last update Tue Mar 17 17:29:31 2015 Alexandre PAGE
*/

#include "serveur.h"

static int	my_nbr_word(char *str, char c)
{
  int		i;
  int		count;

  i = 0;
  count = 0;
  if (!str)
    return (0);
  while (str[i])
    {
      if (str[i] != c)
	count = count + 1;
      while (str[i + 1] != '\0' && str[i] != c)
	i++;
      i++;
    }
  free(str);
  return (count);
}

static int	my_nbr_char(const char *str, int *i, char c)
{
  int		count;

  count = 0;
  while (str[*i] != '\0' && str[*i] != c)
    {
      count = count + 1;
      *i = *i + 1;
    }
  return (count);
}

char		**my_str_to_wordtab(const char *str, char c)
{
  int		i;
  int		j;
  int		nb_word;
  char		**tab;

  i = 0;
  j = 0;
  if ((nb_word = my_nbr_word(strdup(str), c)) == 0)
    return (NULL);
  if ((tab = malloc((nb_word + 1) * sizeof(char **))) == NULL)
    return (NULL);
  while (str[i] != '\0' && nb_word > 0)
    {
      if (str[i] != c)
	{
	  if ((tab[j] = strdup(str + i)) == NULL)
	    return (0);
	  tab[j][my_nbr_char(str, &i, c)] = '\0';
	  j = j + 1;
	  nb_word = nb_word - 1;
	}
      i++;
    }
  tab[my_nbr_word(strdup(str), c)] = '\0';
  return (tab);
}

void	free_str_to_wordtab(char **str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      free(str[i]);
      i = i + 1;
    }
  free(str);
}

int	wordtab_len(char **av)
{
  int	i;

  i = 0;
  while (av[i])
    i = i + 1;
  return (i);
}
