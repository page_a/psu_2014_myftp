/*
** serveur_fork.c for myftp in /home/page_a/rendu/PSU_2014_myftp
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Wed Mar 11 15:30:37 2015 Alexandre PAGE
** Last update Sat Mar 21 22:50:27 2015 Alexandre PAGE
*/

#include "serveur.h"

int	g_connected = 0;

void	xwrite(int fd, char *str)
{
  if (write(fd, str, strlen(str)) == -1)
    {
      printf("Server fail to send data\n");
      exit(EXIT_FAILURE);
    }
}

static void	choose_command(void (*tabptrfn[])(),
			       int client_fd, t_param *param)
{
  int		i;

  if ((i = get_token(param->buff)) != -1)
    {
      if (i == 3 || i == 4)
	tabptrfn[i](client_fd, param->buff, param);
      else if (g_connected)
	tabptrfn[i](client_fd, param->buff, param);
      else
	xwrite(client_fd, "530 Please login with USER and PASS.\n");
    }
  else
    xwrite(client_fd, "500 Unknown command.\n");
}

int	serveur_fork(int client_fd, t_param *param)
{
  void	(*tabptrfn[NBR_FUNC])();
  char	buff[BUFFER_SIZE];

  init_tab(tabptrfn);
  write(client_fd, "220 Welcome Bitch!\n", 19);
  while (42)
    {
      bzero(buff, sizeof(buff));
      read(client_fd, buff, sizeof(buff));
      write(1, buff, sizeof(buff));
      epure_newline(buff);
      param->buff = buff;
      choose_command(tabptrfn, client_fd, param);
    }
  close(client_fd);
  return (0);
}
