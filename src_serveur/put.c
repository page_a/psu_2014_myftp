/*
** stor.c for myftp in /home/page_a/rendu/psu_2014_myftp/serveur
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Fri Mar 20 21:13:30 2015 Alexandre PAGE
** Last update Sat Mar 21 18:54:40 2015 Alexandre PAGE
*/

#include "serveur.h"

extern int	g_socketfd;

static void		upload(int client_fd, int tmp_fd, int file)
{
  char			buff[BUFFER_DU];
  int			my_read;

  xwrite(client_fd,
	 "150 File status okay; about to open data connection.\n");
  while ((my_read = read(tmp_fd, buff, sizeof(buff))) > 0)
    write(file, buff, my_read);
  xwrite(client_fd, "226 Closing data connection.\n");
}

void		put(int client_fd, char *buffer, t_param *param)
{
  int		file;
  int		tmp_fd;
  char		*filename;

  if (!g_socketfd)
    xwrite(client_fd, "425 Use PASV first.\n");
  else
    {
      tmp_fd = accept(g_socketfd, (struct sockaddr *)&param->my.s_in_client,
		      &param->my.s_in_size);
      if (tmp_fd == -1)
	xexit(client_fd);
      filename = get_filename(buffer);
      if ((file = open(filename, O_CREAT | O_TRUNC | O_WRONLY,  S_IRWXU
		       | S_IRWXG | S_IROTH)) == -1)
	xexit(client_fd);
      upload(client_fd, tmp_fd, file);
      close(tmp_fd);
      close(g_socketfd);
      g_socketfd = 0;
    }
}
