/*
** ptfrfunc.c for myftp in /home/page_a/rendu/PSU_2014_myftp/serveur
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Tue Mar 17 16:53:35 2015 Alexandre PAGE
** Last update Sun Mar 22 01:01:27 2015 Alexandre PAGE
*/

#include "serveur.h"

void	init_tab(void (*tabptrfn[])())
{
  tabptrfn[0] = cwd;
  tabptrfn[1] = pwd;
  tabptrfn[2] = quit;
  tabptrfn[3] = login;
  tabptrfn[4] = login;
  tabptrfn[5] = list;
  tabptrfn[6] = pasv;
  tabptrfn[7] = get;
  tabptrfn[8] = put;
  tabptrfn[9] = noop;
  tabptrfn[10] = cdup;
  tabptrfn[11] = help;
  tabptrfn[12] = dele;
}

int	get_token(char *str)
{
  int	i;
  char	*tab[NBR_FUNC + 1];

  tab[0] = "CWD";
  tab[1] = "PWD";
  tab[2] = "QUIT";
  tab[3] = "PASS";
  tab[4] = "USER";
  tab[5] = "LIST";
  tab[6] = "PASV";
  tab[7] = "RETR";
  tab[8] = "STOR";
  tab[9] = "NOOP";
  tab[10] = "CDUP";
  tab[11] = "HELP";
  tab[12] = "DELE";
  tab[13] = NULL;
  i = 0;
  while (tab[i])
    {
      if (!strncmp(tab[i], str, strlen(tab[i])))
	return (i);
      i = i + 1;
    }
  return (-1);
}
