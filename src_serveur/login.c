/*
** login.c for my_ftp in /home/page_a/rendu/PSU_2014_myftp/serveur
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Wed Mar 18 17:11:29 2015 Alexandre PAGE
** Last update Sat Mar 21 18:34:11 2015 Alexandre PAGE
*/

#include "serveur.h"

extern int g_connected;

static char	*lower_case(char *str, int fd_client)
{
  char		*lowerstr;
  int		i;

  i = 0;
  if ( (lowerstr = malloc(sizeof(*str) * strlen(str) + 1)) == NULL)
    xexit(fd_client);
  while (str[i])
    {
      if (str[i] >= 'A' && str[i] <= 'Z')
	lowerstr[i] = str[i] + 32;
      else
	lowerstr[i] = str[i];
      i = i + 1;
    }
  lowerstr[i] = '\0';
  return (lowerstr);
}

static void	login_pass(int fd_client, char *user)
{
  char		buffer[BUFFER_SIZE];

  bzero(buffer, sizeof(buffer));
  read(fd_client, buffer, sizeof(buffer));
  if (!strcmp(user, "anonymous"))
    {
      if (!strncmp("PASS", buffer, 4))
	{
	  g_connected = 1;
	  xwrite(fd_client, "230 Login successful.\n");
	}
    }
  else
    xwrite(fd_client, "530 Login incorrect.\n");
}

static void	login_user(int fd_client, char *buffer)
{
  char		**arg;
  char		*user;

  arg = my_str_to_wordtab(buffer, ' ');
  if (wordtab_len(arg) == 2)
    {
      user = lower_case(arg[1], fd_client);
      xwrite(fd_client, "331 Please specify the password.\n");
      login_pass(fd_client, user);
      free(user);
    }
  else
    xwrite(fd_client, "530 Permission denied.\n");
  free_str_to_wordtab(arg);
}

void		login(int fd_client, char *buffer, t_param *param)
{
  (void)	param;

  if (!strncmp(buffer, "PASS", 3))
    xwrite(fd_client, "332 Login with USER first.\n");
  else
    login_user(fd_client, buffer);
}
