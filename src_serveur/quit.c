/*
** quit.c for myftp in /home/page_a/rendu/PSU_2014_myftp/serveur
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Wed Mar 18 16:55:18 2015 Alexandre PAGE
** Last update Thu Mar 19 12:28:05 2015 Alexandre PAGE
*/

#include "serveur.h"

void		quit(int client_fd, char *buffer, t_param *param)
{
  (void)	buffer;
  (void)	param;

  xwrite(client_fd, "221 Goodbye.\n");
  close(client_fd);
}
