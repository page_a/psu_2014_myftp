/*
** main.c for tp_myftp in /home/page_a/Documents/TP_myftp
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Tue Mar  3 09:12:37 2015 Alexandre PAGE
** Last update Sat Mar 21 14:30:10 2015 Alexandre PAGE
*/

#include "serveur.h"

static void	usage()
{
  printf("Usage : ./serveur port\n");
  exit(EXIT_FAILURE);
}

int	init_socket(t_socks *my, int port)
{
  int		fd;

  my->s_in_size = sizeof(my->s_in_client);
  if (!(my->pe = getprotobyname("TCP")))
    exit(EXIT_FAILURE);
  my->s_in.sin_family = AF_INET;
  my->s_in.sin_port = htons(port);
  my->s_in.sin_addr.s_addr = INADDR_ANY;
  if ((fd = socket(AF_INET, SOCK_STREAM, my->pe->p_proto)) == -1)
    return (-1);
  if ((bind(fd, (const struct sockaddr *) &my->s_in, sizeof(my->s_in))) == -1)
    xexit(fd);
  if (listen(fd, 42) == -1)
    xexit(fd);
  return (fd);
}

void	xexit(int fd)
{
  close(fd);
  exit(EXIT_FAILURE);
}

int			main(int ac, char **av)
{
  t_socks		my;
  int			fd;
  int			port;
  int			client_fd;
  t_param		param;
  pid_t			pid;

  if (ac != 2)
    usage();
  if ((port = atoi(av[1])) == 0)
    usage();
  if ((fd = init_socket(&param.my, port)) == -1)
    usage();
  while (42)
    {
      client_fd = accept(fd, (struct sockaddr *)
			 &my.s_in_client, &my.s_in_size);
      if (client_fd == -1)
	xexit(fd);
      if ((pid = fork()) == 0)
	serveur_fork(client_fd, &param);
    }
  close(fd);
  return (EXIT_SUCCESS);
}
