/*
** pwd.c for myftp in /home/page_a/rendu/PSU_2014_myftp/serveur
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Tue Mar 17 20:59:19 2015 Alexandre PAGE
** Last update Thu Mar 19 12:27:10 2015 Alexandre PAGE
*/

#include "serveur.h"

void		pwd(int client_fd, char *buffer, t_param *param)
{
  (void)	buffer;
  (void)	client_fd;
  (void)	param;
  char		pwd[256];
  char		*tmp;
  char		*root_pwd;
  int		len;

  if ((root_pwd = getenv("PWD")) == NULL)
    xexit(client_fd);
  len = strlen(root_pwd);
  getcwd(pwd, sizeof(pwd));
  if ((tmp = malloc(sizeof(*tmp) * strlen(pwd + len) + 6)) == NULL)
    xexit(client_fd);
  strcpy(tmp, "257 ");
  strcat(tmp, "\"");
  strcat(tmp, pwd + len);
  strcat(tmp, "/\"");
  strcat(tmp, "\n");
  xwrite(client_fd, tmp);
}
