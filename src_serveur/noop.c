/*
** noop.c for myftp in /home/page_a/rendu/psu_2014_myftp/serveur
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Sun Mar 22 00:42:06 2015 Alexandre PAGE
** Last update Sun Mar 22 00:46:43 2015 Alexandre PAGE
*/

#include "serveur.h"

void		noop(int client_fd, char *buffer, t_param *param)
{
  (void)	buffer;
  (void)	param;

  xwrite(client_fd, "200 Command okay.\n");
}
