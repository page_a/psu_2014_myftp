/*
** serveur.h for myftp in /home/page_a/rendu/PSU_2014_myftp
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Wed Mar 11 13:48:40 2015 Alexandre PAGE
** Last update Sat Mar 21 22:38:38 2015 Alexandre PAGE
*/

#ifndef SERVEUR_H_
# define SERVEUR_H_

/* INCLUDES */
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>

/* DEBUG */
#include <errno.h>

/* MACCRO */
# define BUFFER_SIZE 256
# define NBR_FUNC 11
# define BUFFER_DU 4096

/* STRUCTURES */
typedef struct		s_socks
{
  struct sockaddr_in	s_in;
  struct sockaddr_in	s_in_client;
  struct protoent	*pe;
  socklen_t		s_in_size;
}			t_socks;

typedef struct		s_param
{
  char			*client_ip;
  char			*buff;
  t_socks		my;
  t_socks		tmp;
}			t_param;

/* serveur.c */
void	xexit(int);
void	do_client(int);
int	init_socket(t_socks *, int);

/* serveur_fork.c */
int	serveur_fork(int, t_param *);
void	xwrite(int, char *);

/* ptrfunc.c */
void	init_tab(void (*tabptrfn[])());
int	get_token(char *);

/* cwd.c */
void	cwd(int, char *, t_param *);
void	epure_newline(char *);

/* my_str_to_wordtab.c */
int	wordtab_len(char **);
void	free_str_to_wordtab(char **);
char	**my_str_to_wordtab(const char *, char );

/* pwd.c */
void	pwd(int, char *, t_param *);

/* quit.c */
void	quit(int, char *, t_param *);

/* login.c */
void	login(int, char *, t_param *);

/* list.c */
void	list(int, char *, t_param *);

/* pasv.c */
void	pasv(int, char *, t_param *);

/* get.c */
void	get(int, char *, t_param *);
char	*get_filename(char *);

/* stor.c */
void	put(int, char *, t_param *);

/* noop.c */
void	noop(int, char *, t_param *);

/* cdup.c */
void	cdup(int, char *, t_param *);

/* help.c */
void	help(int, char *, t_param *);

/* dele.c */
void	dele(int, char *, t_param *);

#endif /* !SERVEUR_H_ */
