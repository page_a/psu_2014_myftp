/*
** list.c for myftp in /home/page_a/rendu/PSU_2014_myftp/serveur
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Wed Mar 18 23:16:22 2015 Alexandre PAGE
** Last update Sat Mar 21 16:37:06 2015 Alexandre PAGE
*/

#include "serveur.h"

int		g_socketfd = 0;

void		list(int client_fd, char *buffer, t_param *param)
{
  (void)	buffer;
  int		tmp_fd;

  if (!g_socketfd)
    xwrite(client_fd, "425 Use PASV first.\n");
  else
    {
      tmp_fd = accept(g_socketfd, (struct sockaddr *)&param->my.s_in_client,
		      &param->my.s_in_size);
      if (tmp_fd == -1)
  	xexit(client_fd);
      if (fork() == 0)
	{
	  dup2(tmp_fd, 1);
	  execl("/bin/ls", "ls", "-l", NULL);
	}
      close(tmp_fd);
    }
  xwrite(client_fd, "150 Here comes the directory listing.\n");
  xwrite(client_fd, "226 Directory send OK.\n");
  close(g_socketfd);
  g_socketfd = 0;
}
