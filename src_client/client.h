/*
** client.h for myftp in /home/page_a/rendu/PSU_2014_myftp
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Wed Mar 11 17:59:58 2015 Alexandre PAGE
** Last update Sun Mar 22 00:14:19 2015 Alexandre PAGE
*/

#ifndef CLIENT_H_
# define CLIENT_H_

/* INCLUDE */
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/* DEBUG */
#include <errno.h>

/* MACCRO */
# define NBR_FUNC 11
# define BUFFER_SIZE 256
# define PASS_SIZE 32

/* STRUCTURE */
typedef struct		s_socks
{
  struct protoent	*pe;
  struct sockaddr_in	s_in;
}			t_socks;

/* my_str_to_wordtab.c */
void	free_str_to_wordtab(char **);
char	**my_str_to_wordtab(const char *, char c);

/* list.c */
void	list(int, char *);
void	user(int, char *);

/* client.c */
int	create_socket(char *, int);
void	epure_newline(char *);

/* init.c */
void	init_tab(void (*[])());
int	get_token(char *);

/* user.c */
void	user(int, char *);
int	wordtab_len(char **);

/* pwd_exit.c */
void	pwd(int, char *);
void	my_exit(int, char *);

/* get.c */
void	get(int, char *);

/* put.c */
void	put(int, char *);
int	port_pasv(int);
char	*my_request(char *, char *);

/* cd.c */
void	cd(int, char *);

/* noop.c */
void	noop(int, char *);

/* cdup.c */
void	cdup(int, char *);

/* help.c */
void	help(int, char *);

/* dele.c */
void	dele(int, char *);

#endif /* !CLIENT_H_ */
