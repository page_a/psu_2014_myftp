/*
** client_command.c for myftp in /home/page_a/rendu/PSU_2014_myftp
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Wed Mar 11 18:18:46 2015 Alexandre PAGE
** Last update Sun Mar 22 00:25:04 2015 Alexandre PAGE
*/

#include "client.h"

extern char	*ip;

static void	checking_return_list(int fd, char buff[])
{
  bzero(buff, BUFFER_SIZE);
  read(fd, buff, BUFFER_SIZE);
  if (strncmp(buff, "150", 3))
    printf("%s", buff);
  bzero(buff, BUFFER_SIZE);
  read(fd, buff, BUFFER_SIZE);
  if (strncmp(buff, "226", 3))
    printf("%s", buff);
}

void		list(int fd, char *buffer)
{

  (void)	buffer;
  char		buff[BUFFER_SIZE];
  int		fd_new;
  FILE		*file;
  char		*line;
  size_t	len;
  int		port;

  line = NULL;
  len = 0;
  if ((port = port_pasv(fd)) != -1)
    {
      if ((fd_new = create_socket(ip, port)) == -1)
	exit(EXIT_FAILURE);
      write(fd, "LIST\n", 5);
      if ((file = fdopen(fd_new, "r")) == NULL)
	exit(EXIT_FAILURE);
      while (getline(&line, &len, file) != -1)
	printf("%s", line);
      checking_return_list(fd, buff);
    }
  close(fd_new);
}
