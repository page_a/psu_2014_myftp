/*
** pasv.c for myftp in /home/page_a/rendu/psu_2014_myftp/client
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Sun Mar 22 00:04:57 2015 Alexandre PAGE
** Last update Sun Mar 22 00:19:44 2015 Alexandre PAGE
*/

#include "client.h"

int		port_pasv(int fd)
{
  char		**arg;
  char		buff[BUFFER_SIZE];
  int		port;

  write(fd, "PASV\n", 5);
  bzero(buff, sizeof(buff));
  read(fd, buff, sizeof(buff));
  arg = my_str_to_wordtab(buff, ',');
  if (atoi(arg[0]) == 227)
    {
      port = atoi(arg[4]) * 256 + atoi(arg[5]);
      free_str_to_wordtab(arg);
      return (port);
    }
  else
    {
      write(1, buff, sizeof(buff));
      free_str_to_wordtab(arg);
      return (-1);
    }
}
