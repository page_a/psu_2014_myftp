/*
** transfer.c for myftp in /home/page_a/rendu/PSU_2014_myftp/client
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Sun Mar 15 15:40:38 2015 Alexandre PAGE
** Last update Sat Mar 21 21:17:22 2015 Alexandre PAGE
*/

#include "client.h"

extern char	*ip;

static void	check_get(int fd, int fd_new, int fd_dest)
{
  char		buff[BUFFER_SIZE];

  bzero(buff, sizeof(buff));
  read(fd, buff, sizeof(buff));
  if (!strncmp("150", buff, 3))
    printf("Return : %s", buff);
  bzero(buff, sizeof(buff));
  read(fd, buff, sizeof(buff));
  if (!strncmp("226", buff, 3))
    printf("Return : %s", buff);
  close(fd_new);
  close(fd_dest);
}

static char	*get_command(char *str)
{
  char		*s;

  if ((s = malloc(sizeof(*s) * strlen(str) + 8)) == NULL)
    exit(EXIT_FAILURE);
  strcpy(s, "RETR ");
  strcat(s, str);
  strcat(s, "\n");
  return (s);
}

static void	analyse_get(char **arg, int fd_new, int fd_dest, int fd)
{
  char		*line;
  size_t	len;
  FILE		*file;
  char		buff[BUFFER_SIZE];
  char		*tmp;
  int		my_read;

  line = NULL;
  len = 0;
  tmp = get_command(arg[1]);
  write(fd, tmp, strlen(tmp));
  bzero(buff, sizeof(buff));
  while ((my_read = read(fd_new, buff, sizeof(buff))) > 0)
    {
      write(fd_dest, buff, my_read);
      bzero(buff, sizeof(buff));
    }
  if ((file = fdopen(fd_new, "r")) == NULL)
    exit(EXIT_FAILURE);
  while (getline(&line, &len, file) != -1)
    printf("%s", line);
  check_get(fd, fd_new, fd_dest);
}

void		get(int fd, char *buffer)
{
  (void)	buffer;
  char		buff[BUFFER_SIZE];
  int		fd_new;
  int		fd_dest;
  char		**av;
  int		port;

  if ((port = port_pasv(fd)) != -1)
    {
      if ((fd_new = create_socket(ip, port)) == -1)
	exit(EXIT_FAILURE);
      av = my_str_to_wordtab(buffer, ' ');
      if (wordtab_len(av) >= 2)
	{
	  av[1][strlen(av[1]) - 1] = '\0';
	  if ((fd_dest = open(av[1], O_CREAT | O_WRONLY, S_IRWXU
			      | S_IRWXG | S_IROTH)) == -1)
	    exit(EXIT_FAILURE);
	  analyse_get(av, fd_new, fd_dest, fd);
	  free_str_to_wordtab(av);
	}
    }
  else
    printf("Error : %s\n", buff);
  close(fd_new);
}
