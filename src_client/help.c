/*
** help.c for myftp in /home/page_a/rendu/psu_2014_myftp/client
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Sun Mar 22 00:29:49 2015 Alexandre PAGE
** Last update Sun Mar 22 00:54:58 2015 Alexandre PAGE
*/

#include "client.h"

void		help(int client_fd, char *buffer)
{
  char		buff[BUFFER_SIZE];
  int		flag;
  int		size;
  (void)	buffer;

  write(client_fd, "HELP\n", 5);
  bzero(buff, sizeof(buff));
  flag = 0;
  while (flag != 2)
    {
      size = read(client_fd, buff, sizeof(buff));
      if (!strncmp(buff, "214", 3))
	flag += 1;
      else
	write(1, buff, size);
    }
}
