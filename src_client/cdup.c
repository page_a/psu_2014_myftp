/*
** cdup.c for myftp in /home/page_a/rendu/psu_2014_myftp/client
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Sun Mar 22 00:25:42 2015 Alexandre PAGE
** Last update Sun Mar 22 00:29:36 2015 Alexandre PAGE
*/

#include "client.h"

void		cdup(int client_fd, char *buffer)
{
  (void)	buffer;
  char		buff[BUFFER_SIZE];

  write(client_fd, "CDUP\n", 5);
  bzero(buff, sizeof(buff));
  read(client_fd, buff, sizeof(buff));
  write(1, buff, sizeof(buff));
}
