/*
** put.c for myftp in /home/page_a/rendu/psu_2014_myftp/client
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Fri Mar 20 21:31:30 2015 Alexandre PAGE
** Last update Sat Mar 21 21:16:05 2015 Alexandre PAGE
*/

#include "client.h"

extern char	*ip;

char		*my_request(char *buffer, char *str)
{
  char		**arg;
  char		*tmp;

  epure_newline(buffer);
  arg = my_str_to_wordtab(buffer, ' ');
  tmp = NULL;
  if (wordtab_len(arg) >= 2)
    {
      if ((tmp = malloc(sizeof(*tmp) * strlen(arg[1]) + 7)) == NULL)
	exit(EXIT_FAILURE);
      strcpy(tmp, str);
      strcat(tmp, arg[1]);
      strcat(tmp, "\n");
    }
  free_str_to_wordtab(arg);
  return (tmp);
}

static char	*get_filename(char *buffer)
{
  char		**arg;
  char		*str;

  arg = my_str_to_wordtab(buffer, ' ');
  epure_newline(buffer);
  str = strdup(arg[1]);
  free_str_to_wordtab(arg);
  return (str);
}

static int init_put(int fd, char *request)
{
  char	buff[BUFFER_SIZE];

  write(fd, request, strlen(request));
  bzero(buff, sizeof(buff));
  read(fd, buff, sizeof(buff));
  write(1, buff, sizeof(buff));
  if (!strncmp(buff, "150", 3))
    return (1);
  else
    return (-1);
}

static void	transfer_finish(int fd, int fd_new, int file_fd)
{
  char		buff[BUFFER_SIZE];

  close(fd_new);
  close(file_fd);
  bzero(buff, sizeof(buff));
  read(fd, buff, sizeof(buff));
  write(1, buff, sizeof(buff));
}

void		put(int fd, char *buffer)
{
  int		port;
  int		fd_new;
  int		file_fd;
  char		*filename;
  char		buff[BUFFER_SIZE];
  int		my_read;
  char		*request;

  if ((port = port_pasv(fd)) != -1)
    {
      if ((fd_new = create_socket(ip, port)) == -1)
	exit(EXIT_FAILURE);
      if ((request = my_request(buffer, "STOR ")) != NULL
	  && init_put(fd, request) == 1)
	{
	  filename = get_filename(buffer);
	  if ((file_fd = open(filename, O_RDONLY)) == -1)
	    exit(EXIT_FAILURE);
	  while ((my_read = read(file_fd, buff, sizeof(buff))) > 0)
	    write(fd_new, buff, my_read);
	  transfer_finish(fd, fd_new, file_fd);
	}
    }
}
