/*
** pwd.c for my_ftp in /home/page_a/rendu/PSU_2014_myftp/client
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Fri Mar 13 19:38:33 2015 Alexandre PAGE
** Last update Sat Mar 21 17:17:46 2015 Alexandre PAGE
*/

#include "client.h"

void		pwd(int fd, char *buffer)
{
  char		buff[BUFFER_SIZE];
  char		**av;
  (void)	buffer;

  write(fd, "PWD\n", 4);
  bzero(buff, sizeof(buff));
  read(fd, buff, sizeof(buff));
  av = my_str_to_wordtab(buff, ' ');
  if (wordtab_len(av) == 2)
    {
      if (!strncmp(av[0], "257", 3))
	printf("%s", buff);
    }
  else
    printf("%s", buff);
  free_str_to_wordtab(av);
}

void	my_exit(int fd, char *buffer)
{
  (void) buffer;
  char	buff[BUFFER_SIZE];

  write(fd, "QUIT\n", 5);
  bzero(buff, sizeof(buff));
  read(fd, buff, sizeof(buff));
  write(1, buff, sizeof(buff));
  close(fd);
  exit(EXIT_SUCCESS);
}
