/*
** noop.c for myftp in /home/page_a/rendu/psu_2014_myftp/client
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Sun Mar 22 00:20:28 2015 Alexandre PAGE
** Last update Sun Mar 22 00:23:38 2015 Alexandre PAGE
*/

#include "client.h"

void		noop(int client_fd, char *buffer)
{
  char		buff[BUFFER_SIZE];
  (void)	buffer;

  write(client_fd, "NOOP\n", 5);
  bzero(buff, sizeof(buff));
  read(client_fd, buff, sizeof(buff));
  write(1, buff, sizeof(buff));
}
