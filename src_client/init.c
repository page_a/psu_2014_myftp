/*
** init.c for myftp in /home/page_a/rendu/PSU_2014_myftp
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Thu Mar 12 16:41:51 2015 Alexandre PAGE
** Last update Sun Mar 22 00:33:48 2015 Alexandre PAGE
*/

#include "client.h"

void	init_tab(void (*tabptrfn[])())
{
  tabptrfn[0] = list;
  tabptrfn[1] = cd;
  tabptrfn[2] = user;
  tabptrfn[3] = pwd;
  tabptrfn[4] = my_exit;
  tabptrfn[5] = get;
  tabptrfn[6] = put;
  tabptrfn[7] = noop;
  tabptrfn[8] = cdup;
  tabptrfn[9] = help;
  tabptrfn[10] = dele;
}

int	get_token(char *str)
{
  int	i;
  char	*tab[NBR_FUNC + 1];

  tab[0] = "ls";
  tab[1] = "cd";
  tab[2] = "user";
  tab[3] = "pwd";
  tab[4] = "quit";
  tab[5] = "get";
  tab[6] = "put";
  tab[7] = "NOOP";
  tab[8] = "CDUP";
  tab[9] = "HELP";
  tab[10] = "DELE";
  tab[11] = NULL;
  i = 0;
  while (tab[i])
    {
      if (!strncmp(tab[i], str, strlen(tab[i])))
	return (i);
      i = i + 1;
    }
  return (-1);
}
