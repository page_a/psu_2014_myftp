/*
** user.c for my_ftp in /home/page_a/rendu/PSU_2014_myftp/client
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Fri Mar 13 18:59:48 2015 Alexandre PAGE
** Last update Sat Mar 21 18:24:57 2015 Alexandre PAGE
*/

#include "client.h"

static void	cancat(char *tmp, char *arg, char *arg2)
{
  strcpy(tmp, arg);
  strcat(tmp, arg2);
  strcat(tmp, "\n");
}

static void	check_pass(int fd)
{
  char		*tmp;
  char		buff[BUFFER_SIZE];
  char		pass[PASS_SIZE];

  if ((tmp = malloc(sizeof(*tmp) * (7 + PASS_SIZE))) == NULL)
    exit(EXIT_FAILURE);
  write(1, "Password ?", 10);
  bzero(pass, sizeof(pass));
  read(0, pass, sizeof(pass));
  cancat(tmp, "PASS ", pass);
  write(fd, tmp, strlen(tmp));
  bzero(buff, sizeof(buff));
  read(fd, buff, sizeof(buff));
  write(1, buff, sizeof(buff));
  free(tmp);
}

void	user(int fd, char *buffer)
{
  char	**av;
  char	*user;
  char	*tmp;
  char	buff[BUFFER_SIZE];

  av = my_str_to_wordtab(buffer, ' ');
  if (wordtab_len(av) == 2)
    {
      user = av[1];
      if ((tmp = malloc(sizeof(*tmp) * (7 + strlen(user)))) == NULL)
	exit(EXIT_FAILURE);
      cancat(tmp, "USER ", user);
      write(fd, tmp, strlen(tmp));
      free(tmp);
      bzero(buff, sizeof(buff));
      read(fd, buff, sizeof(buff));
      if (!strncmp("331", buff, 3))
	check_pass(fd);
      else
	printf("Error during connection (step1)\n%s\n", buff);
    }
  else
    printf("Error usage: user username\n");
  free_str_to_wordtab(av);
}
