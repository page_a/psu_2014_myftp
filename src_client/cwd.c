/*
** cwd.c for myftp in /home/page_a/rendu/psu_2014_myftp/client
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Sun Mar 22 00:24:43 2015 Alexandre PAGE
** Last update Sun Mar 22 00:24:55 2015 Alexandre PAGE
*/

#include "client.h"

void	cd(int fd, char *buffer)
{
  char	*str;
  (void) fd;

  if ((str = malloc(sizeof(*str) * 6 + strlen(buffer))) == NULL)
    exit(EXIT_FAILURE);
  strcpy(str, "CWD");
  strcat(str, buffer + 2);
  write(fd, str, strlen(str));
  bzero(buffer, 128);
  read(fd, buffer, 128);
  printf("Return : %s", buffer);
}
