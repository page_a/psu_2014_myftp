/*
** client.c for myftp in /home/page_a/rendu/PSU_2014_myftp
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Wed Mar 11 22:27:44 2015 Alexandre PAGE
** Last update Sat Mar 21 21:14:21 2015 Alexandre PAGE
*/

#include "client.h"

char	*ip;

static void	usage()
{
  printf("Usage : ./client machine port\n");
  exit(EXIT_FAILURE);
}

int			create_socket(char *ip, int port)
{
  int			fd;
  struct protoent       *pe;
  struct sockaddr_in    sin;

  if (!(pe = getprotobyname("TCP")))
    return (-1);
  sin.sin_family = AF_INET;
  sin.sin_port = htons(port);
  sin.sin_addr.s_addr = inet_addr(ip);
  if ((fd = socket(AF_INET, SOCK_STREAM, pe->p_proto)) == -1)
    return (-1);
  if (connect(fd, (const struct sockaddr *)&sin, sizeof(sin)) == -1)
    {
      close(fd);
      return (-1);
    }
  return (fd);
}

void    epure_newline(char *str)
{
  int   i;

  i = 0;
  while (str[i])
    {
      if (str[i] == '\n')
        str[i] = '\0';
      i = i + 1;
    }
}

static void communicate(char buff[], int fd, void (*tabptrfn[])())
{
  int			i;

  bzero(buff, BUFFER_SIZE);
  write(1, ">", 1);
  read(0, buff, BUFFER_SIZE);
  if ((i = get_token(buff)) != -1)
    {
      i = get_token(buff);
      tabptrfn[i](fd, buff);
    }
  else
    {
      write(fd, buff, BUFFER_SIZE);
      read(fd, buff, BUFFER_SIZE);
      write(1, buff, BUFFER_SIZE);
    }
}

int			main(int ac, char **av)
{
  int			fd;
  char			buff[BUFFER_SIZE];
  void			(*tabptrfn[NBR_FUNC])();

  if (ac != 3)
    usage();
  ip = av[1];
  init_tab(tabptrfn);
  if ((fd = create_socket(av[1], atoi(av[2]))) == -1)
    usage();
  bzero(buff, sizeof(buff));
  read(fd, buff, sizeof(buff));
  write(1, buff, sizeof(buff));
  while (42)
    communicate(buff, fd, tabptrfn);
  close(fd);
  return (1);
}
