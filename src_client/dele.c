/*
** dele.c for myftp in /home/page_a/rendu/psu_2014_myftp/client
** 
** Made by Alexandre PAGE
** Login   <page_a@epitech.net>
** 
** Started on  Sun Mar 22 00:34:06 2015 Alexandre PAGE
** Last update Sun Mar 22 00:40:04 2015 Alexandre PAGE
*/

#include "client.h"

void		dele(int client_fd, char *buffer)
{
  char		*request;
  char		buff[BUFFER_SIZE];

  if ((request = my_request(buffer, "DELE ")) != NULL)
    {
      write(client_fd, request, strlen(request));
      bzero(buff, sizeof(buff));
      read(client_fd, buff, sizeof(buff));
      write(1, buff, sizeof(buff));
      free(request);
    }
}
